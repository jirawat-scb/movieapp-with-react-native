import {Movie} from '../../types';

interface FavoriteState extends Array<Movie> {}

export default FavoriteState;
