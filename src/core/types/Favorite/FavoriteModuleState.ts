import FavoriteState from './FavoriteState';

interface FavoriteModuleState {
  favoriteMovie: FavoriteState;
}

export default FavoriteModuleState;
