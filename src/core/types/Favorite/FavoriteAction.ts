import FavoriteActionType from './FavoriteActionType';
import {Movie} from '../../types';

export interface FavoriteAddAction {
  type: FavoriteActionType.Add;
  payload: Movie;
}

export interface FavoriteRemoveAction {
  type: FavoriteActionType.Remove;
  payload: Movie;
}

export type FavoriteAction = FavoriteAddAction | FavoriteRemoveAction;
