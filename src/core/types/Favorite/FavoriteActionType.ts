enum FavoriteAction {
  Add = 'Favorite/Add',
  Remove = 'Favorite/Remove',
}

export default FavoriteAction;
