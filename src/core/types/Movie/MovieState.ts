export interface Movie {
  id: number;
  overview: string;
  poster_path: string;
  release_date: string;
  title: string;
  vote_average: number;
}

interface MovieState extends Array<Movie> {}

export default MovieState;
