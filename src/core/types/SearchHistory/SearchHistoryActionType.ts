enum SearchHistoryActionType {
  Add = 'SearchHistory/Add',
}

export default SearchHistoryActionType;
