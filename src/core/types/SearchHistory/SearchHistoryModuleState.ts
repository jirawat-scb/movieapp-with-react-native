import SearchHistoryState from './SearchHistoryState';

interface SearchHistoryModuleState {
  searchHistory: SearchHistoryState;
}

export default SearchHistoryModuleState;
