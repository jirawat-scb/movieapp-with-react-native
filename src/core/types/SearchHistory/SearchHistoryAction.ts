import SearchHistoryActionType from './SearchHistoryActionType';

export interface SearchHistoryAddAction {
  type: SearchHistoryActionType.Add;
  payload: string;
}

export type SearchHistoryAction = SearchHistoryAddAction;
