import {AnyAction, Reducer} from 'redux';
import {IModule} from 'redux-dynamic-modules';
import {
  FavoriteAction,
  FavoriteActionType,
  FavoriteModuleState,
  FavoriteState,
} from '../../types';

export const initialState: FavoriteState = [];

const favoriteReducer: Reducer<FavoriteState, FavoriteAction> = (
  state = initialState,
  action,
) => {
  switch (action.type) {
    case FavoriteActionType.Add:
      return [...state, action.payload];
    case FavoriteActionType.Remove:
      return state.filter((movie) => movie.id !== action.payload.id);
    default:
      return state;
  }
};

const favoriteModule: IModule<FavoriteModuleState> = {
  id: 'favoriteModule',
  reducerMap: {
    favoriteMovie: favoriteReducer as Reducer<FavoriteState, AnyAction>,
  },
  initialActions: [],
};

export default favoriteModule;
