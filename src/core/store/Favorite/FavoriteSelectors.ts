import {FavoriteModuleState} from '../../types';

export function getFavoriteList(state: FavoriteModuleState) {
  return state.favoriteMovie;
}

export default {getFavoriteList};
