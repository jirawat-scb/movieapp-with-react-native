import {
  FavoriteAddAction,
  FavoriteRemoveAction,
  FavoriteActionType,
  Movie,
} from '../../types';

export function addFavorite(movie: Movie): FavoriteAddAction {
  return {
    type: FavoriteActionType.Add,
    payload: movie,
  };
}

export function removeFavorite(movie: Movie): FavoriteRemoveAction {
  return {
    type: FavoriteActionType.Remove,
    payload: movie,
  };
}

export default {addFavorite, removeFavorite};
