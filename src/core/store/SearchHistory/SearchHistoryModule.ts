import {AnyAction, Reducer} from 'redux';
import {IModule} from 'redux-dynamic-modules';
import {
  SearchHistoryAction,
  SearchHistoryActionType,
  SearchHistoryModuleState,
  SearchHistoryState,
} from '../../types';

export const initialState: SearchHistoryState = [];

const searchHistoryReducer: Reducer<SearchHistoryState, SearchHistoryAction> = (
  state = initialState,
  action,
) => {
  switch (action.type) {
    case SearchHistoryActionType.Add:
      if (!state.includes(action.payload)) {
        return [action.payload, ...state];
      }
    default:
      return state;
  }
};

const searchHistoryModule: IModule<SearchHistoryModuleState> = {
  id: 'searchHistoryModule',
  reducerMap: {
    searchHistory: searchHistoryReducer as Reducer<
      SearchHistoryState,
      AnyAction
    >,
  },
  initialActions: [],
};

export default searchHistoryModule;
