import {SearchHistoryAddAction, SearchHistoryActionType} from '../../types';

export function addSearchHistory(title: string): SearchHistoryAddAction {
  return {
    type: SearchHistoryActionType.Add,
    payload: title,
  };
}

export default {addSearchHistory};
