import {SearchHistoryModuleState} from '../../types';

export function getSearchHistoryList(state: SearchHistoryModuleState) {
  return state.searchHistory;
}

export default {getSearchHistoryList};
