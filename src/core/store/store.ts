import {createStore, IModuleStore} from 'redux-dynamic-modules';
import {getThunkExtension} from 'redux-dynamic-modules-thunk';
import {SearchHistoryModuleState, FavoriteModuleState} from '../types';
import {searchHistoryModule, searchHistorySelectors} from './SearchHistory';
import {favoriteModule, favoriteSelectors} from './Favorite';

export interface RootStore
  extends SearchHistoryModuleState,
    FavoriteModuleState {}

const extensions = [getThunkExtension()];

export const store: IModuleStore<RootStore> = createStore(
  {
    initialState: {},
    extensions,
  },
  searchHistoryModule,
  favoriteModule,
);

export {searchHistoryModule, searchHistorySelectors};
export {favoriteModule, favoriteSelectors};
export default store;
