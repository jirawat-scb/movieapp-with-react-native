import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  card: {
    borderBottomWidth: 1,
    borderBottomColor: '#D3D3D3',
    marginHorizontal: 20,
    paddingVertical: 10,
  },
  cardContent: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingTop: 2,
    paddingBottom: 7,
  },
  row: {
    flexDirection: 'row',
  },
  tinyPoster: {
    width: 92,
    height: 138,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  releaseDate: {
    color: 'grey',
  },
  description: {
    marginTop: 10,
  },
});

export default styles;
