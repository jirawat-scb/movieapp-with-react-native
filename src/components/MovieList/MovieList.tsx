import React from 'react';
import {View, Text, FlatList, TouchableOpacity, Image} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import {MovieState} from '../../core';
import styles from './styles';

const MovieList = (props: {data: MovieState}) => {
  const {data} = props;
  const navigation = useNavigation();

  return (
    <FlatList
      style={styles.container}
      data={data}
      keyExtractor={(item) => item.id.toString()}
      renderItem={({item}) => {
        return (
          <TouchableOpacity
            style={[styles.card, styles.row]}
            onPress={() => {
              navigation.navigate('Movie Detail', {item});
            }}>
            <Image
              style={styles.tinyPoster}
              source={{
                uri: 'https://image.tmdb.org/t/p/w92' + item.poster_path,
              }}
            />
            <View style={styles.cardContent}>
              <Text style={styles.title}>{item.title}</Text>
              <Text style={styles.releaseDate}>{item.release_date}</Text>
              <Text style={styles.description} numberOfLines={4}>
                {item.overview}
              </Text>
            </View>
          </TouchableOpacity>
        );
      }}
    />
  );
};

export default MovieList;
