import React from 'react';
import {useSelector} from 'react-redux';

import {MovieList} from '../components';
import {MovieState} from '../core';

const FavoriteScreen = () => {
  const favoriteMovie = useSelector(
    (state: {favoriteMovie: MovieState}) => state.favoriteMovie,
  );

  return <MovieList data={favoriteMovie} />;
};

export default FavoriteScreen;
