import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {View, Text, Image, Button, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import {favoriteSelectors, favoriteActions} from '../../core';
import {Movie} from '../../core';
import styles from './styles';

interface Props {
  route: {
    params: {
      item: Movie;
    };
  };
}

const DetailScreen = (props: Props) => {
  const {item} = props.route.params;

  const favoriteMovie = useSelector(favoriteSelectors.getFavoriteList);
  const dispatch = useDispatch();
  const navigation = useNavigation();

  navigation.setOptions({
    headerRight: () => (
      <Button
        title="Search"
        onPress={() => {
          navigation.navigate('Movie Search');
        }}
      />
    ),
  });

  let isFavorite: boolean = false;
  for (const movie of favoriteMovie) {
    if (movie.id === item.id) {
      isFavorite = true;
      break;
    }
  }

  return (
    <View style={styles.container}>
      <Image
        style={styles.largePoster}
        source={{
          uri: 'https://image.tmdb.org/t/p/w92' + item.poster_path,
        }}
      />
      <View style={styles.content}>
        <Text style={styles.title}>{item.title}</Text>
        <Text>Vote average: {item.vote_average}</Text>
        <Text style={styles.description}>{item.overview}</Text>
      </View>
      <TouchableOpacity
        style={styles.button}
        onPress={() => {
          isFavorite
            ? dispatch(favoriteActions.removeFavorite(item))
            : dispatch(favoriteActions.addFavorite(item));
        }}>
        <Text style={styles.textButton}>
          {isFavorite ? 'Unfavorite' : 'Favorite'}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default DetailScreen;
