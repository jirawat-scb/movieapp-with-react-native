import React, {useState, useEffect, useCallback} from 'react';

import MovieAPI from '../api/MovieAPI';
import {MovieList} from '../components';

interface Props {
  route: {
    params: {
      title: string;
    };
  };
}

const MovieListScreen = (props: Props) => {
  const {title} = props.route.params;
  const [dataMovie, setDataMovie] = useState([]);

  const fetchMovie = useCallback(async () => {
    const searchTerm = title;
    const response = await MovieAPI.get('/api/movies/search', {
      params: {
        query: searchTerm,
      },
    });

    setDataMovie(response.data.results);
  }, [title]);

  useEffect(() => {
    fetchMovie();
  }, [fetchMovie]);

  return <MovieList data={dataMovie} />;
};

export default MovieListScreen;
