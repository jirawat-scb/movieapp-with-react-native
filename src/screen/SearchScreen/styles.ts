import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
  },
  textInput: {
    height: 40,
    borderWidth: 1,
    borderRadius: 20,
    borderColor: '#BBBBBB',
    marginVertical: 20,
    paddingHorizontal: 15,
  },
  touchableStyle: {
    justifyContent: 'center',
    height: 40,
    paddingVertical: 5,
    borderBottomWidth: 1,
    borderBottomColor: '#D3D3D3',
  },
  title: {
    paddingBottom: 5,
  },
});

export default styles;
