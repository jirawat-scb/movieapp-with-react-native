import React, {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {
  View,
  Text,
  TextInput,
  Button,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import {searchHistorySelectors, searchHistoryActions} from '../../core';
import styles from './styles';

const SearchScreen = () => {
  const searchHistory = useSelector(
    searchHistorySelectors.getSearchHistoryList,
  );

  const [title, setTitle] = useState('');
  const dispatch = useDispatch();
  const navigation = useNavigation();

  navigation.setOptions({
    headerRight: () => (
      <Button
        title="Favorite"
        onPress={() => {
          navigation.navigate('Favorite Movie');
        }}
      />
    ),
  });

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.textInput}
        placeholder="Search.."
        value={title}
        keyboardType="web-search"
        onChangeText={(text) => setTitle(text)}
        onSubmitEditing={() => {
          if (title) {
            navigation.navigate('Movie List', {title});
            dispatch(searchHistoryActions.addSearchHistory(title));
          }
        }}
      />
      <Text style={styles.title}>Search History:</Text>
      <FlatList
        data={searchHistory}
        keyExtractor={(index) => index.toString()}
        renderItem={({item}) => {
          return (
            <TouchableOpacity
              style={styles.touchableStyle}
              onPress={() => {
                navigation.navigate('Movie List', {title: item});
              }}>
              <Text>{item}</Text>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
};

export default SearchScreen;
