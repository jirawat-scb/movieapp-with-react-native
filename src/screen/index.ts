export {default as SearchScreen} from './SearchScreen/SearchScreen';
export {default as MovieListScreen} from './MovieListScreen';
export {default as DetailScreen} from './DetailScreen/DetailScreen';
export {default as FavoriteScreen} from './FavoriteScreen';
