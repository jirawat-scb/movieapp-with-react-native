import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {Provider} from 'react-redux';

import {store} from './src/core';
import {
  SearchScreen,
  MovieListScreen,
  DetailScreen,
  FavoriteScreen,
} from './src/screen';

const Stack = createStackNavigator();

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Movie Search">
          <Stack.Screen name="Movie Search" component={SearchScreen} />
          <Stack.Screen name="Movie List" component={MovieListScreen} />
          <Stack.Screen name="Movie Detail" component={DetailScreen} />
          <Stack.Screen name="Favorite Movie" component={FavoriteScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
